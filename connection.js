const Redis = require('ioredis');
const mqtt = require('mqtt');

const host = 'localhost';
const port = '1883';

const redis = new Redis({
    host: '127.0.0.1',
    port: '6379',
});

const mqttconfig = {
  host: "127.0.0.1",
  port: 1883,
  username: "admin",
  password: "server$4321",
  qos: 2,
};

const client = mqtt.connect(mqttconfig);

module.exports = {redis, client};