const mqtt = require("mqtt");
const moment = require("moment");

const devID = "INEM_DEMO";
const topic = `devicesIn/${devID}/data`;
const max = 100;
const min = 10;
let mqttClient;

function publishData() {
  const interval = setInterval(() => {
    /** Publish on mqtt every second */

    console.log("Publishing ", devID, " data...");
    const random = Math.random() * (max - min) + min;
    const random2 = Math.random() * (max - min) + min;
    const random3 = Math.random() * (max - min) + min;
    console.log("Random number", random);

    const currentHour = moment().get("hour"); // should be either 5(10:30 am in IST) or 12 (5:30 pm in IST) publish low VOLTS1 value

    const dataPacket = {
      device: devID,
      time: Date.now(),
      data: [
        {
          tag: "ACTIVE",
          value: random,
        },
      ],
    };

    mqttClient.publish(topic, JSON.stringify(dataPacket));
  }, 4000);
}

const mqttconfig = {
  host: "127.0.0.1",

  port: 1883,

  username: "admin",

  password: "server$4321",

  qos: 2,
};

mqttconfig.clientId = "DMFM_D2" + Date.now();

mqttClient = mqtt.connect(mqttconfig);

console.log("EnergyMeter Mqtt client connected:-", mqttClient.options.clientId);

publishData();
