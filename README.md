# Redis MQTT emqx Device Management

Device Management => Checking If the Device is active or Inactive in certain time interval until  the next data packet is published by the Publisher (Device). 

## Getting Started

To run the application on your local machine you need to have node.js and mongodb installed, up and running. Once you have installed node.js and mongodb, you can now download this repository as .zip and extract it into a folder. After extracting, open the repository directory in a terminal / cmd and run the following command:

```
npm init
```

Application should now be up and running at port 3000. To access the application simply open your preffered browser and type the following in the url bar:

```
http://localhost:3000
```

To stop the server press Ctrl + C on the server terminal.

## Reference 

https://redis.io/commands/?group=hash <br/>
https://www.sitepoint.com/using-redis-node-js/
