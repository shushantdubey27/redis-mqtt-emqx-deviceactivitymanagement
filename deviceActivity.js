const { redis, client } = require("./connection");
const mqtt = require("mqtt");

const devID = "INEM_DEMO";
const topic = `devicesIn/${devID}/data`;


client.on("connect", async () => {
  client.subscribe(topic, () => {
    console.log(`Subscribe to topic ${topic}`);
  });
  let exists = await redis.hget("DefaultTimeout", devID);
  if(!exists) {
    await redis.hset("DefaultTimeout", devID, 5000, (error) => {
      if (error) throw error;
    });
  }
});



client.on("message", async (topic, message) => {
  const resObj = JSON.parse(message.toString());
  
  let defaultTime = await redis.hget("DefaultTimeout", devID); // 5000
  console.log(defaultTime);
  let publisherTime = resObj.time;  // published time

  let lastTimeNoted = await redis.hget("Device", "time", (error) => {
    if (error) throw error;
  }) 
  let differnce = publisherTime - lastTimeNoted;
  console.log(defaultTime, publisherTime, differnce);

  if(differnce < defaultTime) {
    console.log("Device is Active");
  }
  else {
    console.log("Device is InActive");
  }
  await redis.hset("Device", resObj, (error) => {
    if (error) throw error;
  });
});

